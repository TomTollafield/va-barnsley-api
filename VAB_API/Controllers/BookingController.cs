﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VAB_API.Models;

namespace VAB_API.Controllers
{
    public class BookingController : ApiController
    {
        [HttpPost]
        public Guid Authenticate([FromBody]AuthModel model)
        {
            var ents = new VABarnsleyEntities();

            var verified = ents.ApiAuthentications.Where(x => x.Password == model.Password && x.Username == model.Username).Any();

            if (verified)
            {
                var ent = ents.ApiAuthentications.Where(x => x.Password == model.Password && x.Username == model.Username).First();
                ent.Token = Guid.NewGuid();
                ent.ExpiryDate = DateTime.Now.AddMinutes(10);
                ents.SaveChanges();

                return ent.Token;
            }
            else
            {
                return Guid.Empty;
            }
        }      

        [HttpPost]
        public List<tblBooking> GetBooking([FromBody]AuthModel model)
        {
            if (model != null)
            {
                var ents = new VABarnsleyEntities();

                var convertedToken = Guid.Parse(model.Token);

                if (ents.ApiAuthentications.Where(x => x.Token == convertedToken).Any())
                {
                    var tokenEnt = ents.ApiAuthentications.Where(x => x.Token == convertedToken).First();

                    if (tokenEnt.ExpiryDate > DateTime.Now)
                    {
                        return ents.tblBookings.ToList();

                    }
                    else
                    {
                        return new List<tblBooking>();
                    }
                }
                else
                {
                    return new List<tblBooking>();
                }
            }else
            {
                return new List<tblBooking>();
            }
        }
    }
}
